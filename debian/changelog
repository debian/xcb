xcb (2.4-8) UNRELEASED; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 05 Sep 2022 18:27:03 -0000

xcb (2.4-7) unstable; urgency=low

  * QA upload.

  [ Debian Janitor ]
  * Use correct machine-readable copyright file URI.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 13 Feb 2021 01:09:09 +0000

xcb (2.4-6) unstable; urgency=medium

  * QA upload.
  * Remove vague "pigeon holes" metaphor from package description.
  * Drop trailing "." from package long description.
  * Correct typo in a patch.
  * Drop unnecessary comment in debian/rules.
  * Bump Standards-Version to 4.5.0.

 -- Chris Lamb <lamby@debian.org>  Sun, 29 Mar 2020 11:26:14 +0100

xcb (2.4-5) unstable; urgency=medium

  * QA upload.
  * Update compat level to 12.
  * Remove entry from d/watch as URL doesnot work.
  * Use dh_auto_build to fix ftcbfs. (Closes: #928317)
    - Thanks Nguyen Van. Hieu
  * use https in d/copyright format url.
  * Update compat level to 12.
  * Update Standards-Version to 4.4.1
  * Add Rules-Requires-Root: no
  * Orphan the package (See: #675764)
  * Add Vcs links to d/control.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sun, 12 Jan 2020 19:43:41 +0000

xcb (2.4-4.3) unstable; urgency=low

  * Non-maintainer upload.
  * Enable all hardened build flags. Patch thanks to Simon Ruderich
    <simon@ruderich.org>.

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 11 May 2012 14:25:15 +0300

xcb (2.4-4.2) unstable; urgency=low

  * Non-maintainer upload.
  * Update to packaging format "3.0 quilt"
  * debian/copyright
    - Update to format 1.0.
  * debian/control
    - Update to Standards-Version to 3.9.3 and debhelper to 9.
    - Add ${misc:Depends}.
    - Remove Homepage; dead upstream.
  * debian/menu
    - Update section from obsolete Apps/Tools to Applications/Graphics.
    - Change the path to the binary in the menu file. This together with a
      rebuild makes xcb work with xorg 7.0. Patch thanks to
      Erik Johansson <debian@ejohansson.se> (Closes: #363172, #585539).
  * debian/patches
     - (01, 02): Convert in-line patches to individual debian/patches.
     - (03): New. Fix hyphens in manual page.
  * debian/rules
    - Migrate to dh(1) dues to hardened build flags.
    - Install manual pages with standard suffix *.1, not *.1x.
    - Use all hardened build flags
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags
  * debian/watch
    - Update to format 3 (Closes: #529149). Patch thanks to Erik Johansson
      <debian@ejohansson.se>.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 26 Apr 2012 18:05:19 +0300

xcb (2.4-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS by replacing xutils with xutils-dev in Build-Depends
    (Closes: #485538).
  * Fix FTBFS by using “rm -f Makefile” instead of “rm Makefile” in the
    clean target (Closes: #533965).
  * debian/control: Move the Homepage where it belongs.

 -- Cyril Brulebois <kibi@debian.org>  Sun, 19 Jul 2009 03:30:22 +0200

xcb (2.4-4) unstable; urgency=low

  * Applied patch from CVS (http://software.schmorp.de/)
    fix by Paul Fox to restore -S functionality that was broken with 2.4.
    (Closes: #250203)

 -- Michael Schiansky <ms@debian.org>  Wed,  9 Jun 2004 13:49:05 +0200

xcb (2.4-3) unstable; urgency=low

  * New Maintainer (now DD)
  * Bumped Standard to 3.6.1 (no changes needed)

 -- Michael Schiansky <ms@debian.org>  Wed, 18 Feb 2004 12:09:34 +0100

xcb (2.4-2) unstable; urgency=low

  * Updated debian/copyright 'cause of lintian warning:
    W: xcb: copyright-lists-upstream-authors-with-dh_make-boilerplate

 -- Michael Schiansky <michael@schiansky.de>  Thu, 11 Sep 2003 03:36:10 +0200

xcb (2.4-1) unstable; urgency=low

  * New upstream release
  * New maintainer (Closes: #187732)
  * Fixed Build-Depends (Closes: #170008)
  * Updated debian/copyright
  * Added xcb-Homepage to debian/control

 -- Michael Schiansky <michael@schiansky.de>  Sat, 23 Aug 2003 04:03:23 +0200

xcb (2.3i-4) unstable; urgency=low

  * Initialised the cut buffers, so that rotating doesn't crash if all
    buffers haven't yet been touched (Closes: #104222)

 -- Cyrille Chepelov <chepelov@calixo.net>  Fri, 20 Jul 2001 16:08:36 +0200

xcb (2.3i-3) unstable; urgency=low

  * I have mis-understood the license (in the paranoider way,
    fortunately). In fact, it's really Free.

 -- Cyrille Chepelov <chepelov@calixo.net>  Tue,  3 Jul 2001 09:39:37 +0200

xcb (2.3i-2) unstable; urgency=low

  * rebuilt with dpkg-dev == 1.9.10.

 -- Cyrille Chepelov <chepelov@calixo.net>  Fri, 29 Jun 2001 22:11:08 +0200

xcb (2.3i-1) unstable; urgency=low

  * Initial Release (closes #97840).

 -- Cyrille Chepelov <chepelov@calixo.net>  Wed, 27 Jun 2001 23:09:40 +0200
